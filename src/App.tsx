import React from 'react';
import './App.css';
import { BrowserRouter, Route } from "react-router-dom";
import {Home} from './pages/Home';
import {Time} from './pages/Time';


function App() {
  return (
    <BrowserRouter>
            <div>

                    <Route path='/'>
                        <Home/>
                    </Route>

                    <Route path='/time'>
                        <Time/>
                    </Route>
                
            </div>
        </BrowserRouter>
  );
}

export default App;
